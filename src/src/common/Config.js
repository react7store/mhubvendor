/** @format */

import Images from './Images'
import Constants from './Constants'
import Icons from './Icons'

export default {
  /**
   Step 1: change to your website URL and the wooCommerce API consumeKey
   */
  WooCommerce: {
    url: 'http://react7.store/mhub/vendor/',
    consumerKey: 'ck_858dc599a239bfa7b10039c14dad65d0734da2a7',
    consumerSecret: 'cs_b47729553c5cd801adf25bcd45bb6a022fccf31b',
  },

  /**
   Step 2: Setting Product Images
   - ProductSize: Explode the guide from: update the product display size: https://mstore.gitbooks.io/mstore-manual/content/chapter5.html
   The default config for ProductSize is disable due to some problem config for most of users.
   If you have success config it from the Wordpress site, please enable to speed up the app performance
   - HorizonLayout: Change the HomePage horizontal layout - https://mstore.gitbooks.io/mstore-manual/content/chapter6.html
   */
  ProductSize: {
    enable: false,
    CatalogImages: {width: 300, height: 360},
    SingleProductImage: {width: 600, height: 720},
    ProductThumbnails: {width: 180, height: 216},
  },
  // BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
  // NOTE: name is define value --> change field in Language.js
  HorizonLayout: [
    {tag: 52, paging: true, layout: Constants.Layout.miniBanner},
    {
      name: 'featureProducts',
      category: 179,
      image: Images.Banner.Feature,
      layout: Constants.Layout.threeColumn,
    },
    {
      name: 'bagsCollections',
      category: 102,
      image: Images.Banner.Banner2,
      layout: Constants.Layout.twoColumn,
    },
    {
      name: 'womanBestSeller',
      category: 130,
      image: Images.Banner.Banner3,
      layout: Constants.Layout.twoColumnHigh,
    },
    {
      name: 'manCollections',
      category: 149,
      image: Images.Banner.Banner1,
      layout: Constants.Layout.card,
    }
  ],

  /**
   step 3: Config image for the Payment Gateway
   Notes:
   - Only the image list here will be shown on the app but it should match with the key id from the WooCommerce Website config
   - It's flexible way to control list of your payment as well
   Ex. if you would like to show only cod then just put one cod image in the list
   * */
  Payments: {
    bacs: require('@images/payment_logo/PayPal.png'),
    cod: require('@images/payment_logo/cash_on_delivery.png'),
    paypal: require('@images/payment_logo/PayPal.png'),
    stripe: require('@images/payment_logo/stripe.png'),
  },

  /**
   Step 4: Advance config:
   - showShipping: option to show the list of shipping method
   - showStatusBar: option to show the status bar, it always show iPhoneX
   - LogoImage: The header logo
   - LogoWithText: The Logo use for sign up form
   - LogoLoading: The loading icon logo
   - appFacebookId: The app facebook ID, use for Facebook login
   - CustomPages: Update the custom page which can be shown from the left side bar (Components/Drawer/index.js)
   - WebPages: This could be the id of your blog post or the full URL which point to any Webpage (responsive mobile is required on the web page)
   - CategoryListView: default layout for category (true/false)
   - intro: The on boarding intro slider for your app
   - menu: config for left menu side items (isMultiChild: This is new feature from 3.4.5 that show the sub products categories)
   * */
  shipping: {
    visible: true,
    time: {
      free_shipping: '4 - 7 Days',
      flat_rate: '1 - 4 Days',
      local_pickup: '1 - 4 Days',
    },
  },
  showStatusBar: true,
  LogoImage: require('@images/new_logo.png'),
  LogoWithText: require('@images/logo_with_text.png'),
  LogoLoading: require('@images/logo.png'),

  showAdmobAds: false,
  AdMob: {
    deviceID: 'pub-2101182411274198',
    unitID: 'ca-app-pub-2101182411274198/4100506392',
    unitInterstitial: 'ca-app-pub-2101182411274198/8930161243',
    isShowInterstital: true,
  },
  appFacebookId: '232462654091155',
  CustomPages: {contact_id: 2508, about_us: 2494},
  WebPages: {marketing: 'http://react7.store/mhub/vendor/'},
  CategoryListView: true,
  intro: [
    {
      key: 'page1',
      title: 'Welcome To MHubVendor',
      text:
        'Welcome to the future of payment.',
      icon: 'ios-basket-outline',
      colors: ['#0FF0B3', '#036ED9'],
    },
    {
      key: 'page2',
      title: 'Discover Limitless Possibilities',
      text:
        'Payment at your fingertips',
      icon: 'ios-card-outline',
      colors: ['#13f1fc', '#0470dc'],
    },
    {
      key: 'page3',
      title: 'A promise of security',
      text: 'Stay safe when doing transactions',
      icon: 'ios-finger-print-outline',
      colors: ['#b1ea4d', '#459522'],
    },
  ],

  /**
   * Config Left Menu Side
   * @param goToScreen 3 Params (routeName, params, isReset = false)
   * BUG: Language can not change when set default value in Config.js ==> pass string to change Languages
   */
  menu: {
    // has child categories
    isMultiChild: true,
    // Unlogged
    listMenuUnlogged: [
      {
        text: 'Login',
        routeName: 'LoginScreen',
        params: {
          isLogout: false,
        },
        icon: Icons.MaterialCommunityIcons.SignIn,
      },
    ],
    // user logged in
    listMenuLogged: [
      {
        text: 'Logout',
        routeName: 'LoginScreen',
        params: {
          isLogout: true,
        },
        icon: Icons.MaterialCommunityIcons.SignOut,
      },
    ],
    // Default List
    listMenu: [
      {
        text: 'Store',
        routeName: 'Default',
        icon: Icons.MaterialCommunityIcons.Home,
      },
      {
        text: 'Inventory',
        routeName: 'NewsScreen',
        icon: Icons.MaterialCommunityIcons.News,
      },
      {
        text: 'Merchant Area',
        routeName: 'CustomPage',
        params: {
          id: 5005,
          title: 'Merchant Area',
        },
        icon: Icons.MaterialCommunityIcons.Pin,
      },
      {
        text: 'Merchant store',
        routeName: 'CustomPage',
        params: {
          id: 2494,
          title: 'About Us',
        },
        icon: Icons.MaterialCommunityIcons.Email,
      },
      {
        text: 'MHub Setting',
        routeName: 'SettingScreen',
        icon: Icons.MaterialCommunityIcons.Setting,
      },
    ],
  },

  // Layout modal select
  LayoutsSwicher: [
    {
      layout: Constants.Layout.card,
      image: Images.icons.iconCard,
      text: 'cardView',
    },
    {
      layout: Constants.Layout.simple,
      image: Images.icons.iconRight,
      text: 'simpleView',
    },
    {
      layout: Constants.Layout.twoColumn,
      image: Images.icons.iconColumn,
      text: 'twoColumnView',
    },
    {
      layout: Constants.Layout.threeColumn,
      image: Images.icons.iconThree,
      text: 'threeColumnView',
    },
    {
      layout: Constants.Layout.horizon,
      image: Images.icons.iconHorizal,
      text: 'horizontal',
    },
    {
      layout: Constants.Layout.advance,
      image: Images.icons.iconAdvance,
      text: 'advanceView',
    },
  ],
}
