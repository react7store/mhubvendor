'use strict'

import React, { Component } from 'react'
import {
  FlatList,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Text,
} from 'react-native'

import css from './style'
import { Languages, Color, Events } from '@common'
import Rating from '@custom/react-native-star-rating'
import {RefAPI} from '@services'
import Icon from '@expo/vector-icons/SimpleLineIcons'
import DropdownAlert from 'react-native-dropdownalert'
import {toast} from '@app/Omni'
import { connect } from 'react-redux'

class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      txtComment: '',
      addComment: false,
      starCount: 0,
      statusRate: 'Very Good',
    }
  }

  // componentWillMount() {
  //   Events.onCommentShowError(this.showError)
  // }

  // showError = message => {
  //   this.dropdown && this.dropdown.alertWithType('error', 'Error', message)
  // }


  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
    })
  }

  submitComment = () => {
    const { userData, cookie, post } = this.props
    var self = this
    if (this.state.txtComment == '') {
      return toast(Languages.errInputComment)
    }
    if (this.state.starCount == 0) {
      return toast(Languages.errRatingComment)
    }
    // console.log(post)
    let commentData = {
      post_id: post.id,
      content: this.state.txtComment,
      cookie,
      meta: JSON.stringify({
        rating: this.state.starCount,
        verified: 0,
      }),
    }
    RefAPI.createComment(commentData).then(data => {
      // console.log('comment data', data);
      if (data.status == 'ok') {
        self.setState({
          addComment: true,
          txtComment: '',
        })
        toast(Languages.thanksForReview)
        Events.closeModalReview()
      }
    })
  }

  render() {
    const renderCommentInput = () => {
      return (
        <View style={{ flex: 1 }}>
          <View style={css.rowHead}>
            <Text style={css.headText}>{Languages.yourcomment}</Text>
          </View>
          <View style={css.inputCommentWrap}>
            <TextInput
              style={css.inputCommentText}
              underlineColorAndroid="transparent"
              autoCorrect={false}
              multiline={true}
              value={this.state.txtComment}
              onChangeText={text => this.setState({ txtComment: text })}
              placeholder={Languages.placeComment}
              onSubmitEditing={this.submitComment.bind(this)}
            />
             </View>
            <TouchableOpacity onPress={this.submitComment} style={css.sendView}>
              <Icon
                name="cursor"
                size={16}
                color="white"
                style={css.sendButton}
              />
              <Text style={css.sendText}>{Languages.send}</Text>
            </TouchableOpacity>

        </View>
      )
    }

    const renderStatusRate = value => {
      switch (value) {
        case 1:
          return 'Terrible'
        case 2:
          return 'Poor'
        case 3:
          return 'Average'
        case 4:
          return 'Very Good'
        case 5:
          return 'Exceptional'
        default:
          return 'Average'
          break
      }
    }

    return (
      <View style={css.wrapComment}>
        <Text style={css.headCommentText}>{Languages.comment}</Text>
        <View style={css.fullWidth}>
          <View style={css.wrapRating}>
            <Rating
              disabled={false}
              maxStars={5}
              starSize={26}
              emptyStar={'star-o'}
              fullStar={'star'}
              // halfStar={'star-half-o'}
              // halfStarEnabled
              rating={this.state.starCount}
              starColor={Color.starRating}
              fullStarColor={Color.starRating}
              halfStarColor={Color.starRating}
              emptyStarColor={'#ccc'}
              selectedStar={rating => this.onStarRatingPress(rating)}
            />
          </View>
          <View style={css.besideStar}>
            <View style={css.statusRate}>
              <Text style={css.textStatusRate}>
                {renderStatusRate(this.state.starCount)}
              </Text>
            </View>
          </View>
        </View>
        {renderCommentInput()}
        {/*<DropdownAlert ref={ref => (this.dropdown = ref)} />*/}
      </View>
    )
  }
}

const mapStateToProps = ({ user }) => {
  return {
    userData: user.user,
    cookie: user.token,
  }
}
export default connect(mapStateToProps)(Index)
