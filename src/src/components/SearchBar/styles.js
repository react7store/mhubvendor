/** @format */

import React, {
  StyleSheet,
  Platform,
  Dimensions,
  PixelRatio,
} from 'react-native'
import { Color, Constants } from '@common'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  searchWrap: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 2,
    backgroundColor: '#fff',
    ...Platform.select({
      ios: {
        shadowColor: '#333',
        shadowOffset: {
          width: 2,
          height: 5,
        },
        shadowRadius: 8,
        shadowOpacity: 0.2,
      },
      android: {
        elevation: 1.5,
      },
    }),
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    tintColor: '#999',
    marginHorizontal: 10,
  },
  input: {
    flex: 1,
    fontSize: 16,
    fontFamily: Constants.fontFamily,
    paddingVertical: 15,
    paddingLeft: 20,
  },
  btnFilSearch: {
    marginHorizontal: 10,
    zIndex: 999,
    marginRight: 15,
    shadowColor: 'rgba(0,0,0, .5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 3,
    shadowOpacity: 0.1,
  },
  iconSearchAdvance: {
    width: 18,
    height: 18,
    tintColor: Color.searchButton,
    resizeMode: 'contain'
  }
})
