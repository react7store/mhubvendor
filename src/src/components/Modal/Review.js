import React, { Component } from 'react'
import {Text} from 'react-native'
import { Events } from '@common'
import { ModalBox, Review} from '@components'
import { Login } from '@containers'

import styles from './styles'
import { connect } from 'react-redux'

class ReviewModal extends Component {
  state = { post: '' }
  componentDidMount() {
    Events.onOpenModalReview(this.open)
    Events.onCloseModalReview(this.close)
  }

  open = ( post ) => {
    this.setState({ post })
    this.modal && this.modal.openModal()
  }

  close = () => this.modal.closeModalLayout()

  render() {
    const { userData } = this.props

    return (
      <ModalBox
        css={styles.boxComment}
        isComment
        ref={modal => (this.modal = modal)}
      >
        {userData == null ? <Login isModal /> : <Review post={this.state.post} />}
      </ModalBox>
    )
  }
}
const mapStateToProps = ({ user }) => {
  return {
    userData: user.user,
  }
}

export default connect(mapStateToProps)(ReviewModal)
