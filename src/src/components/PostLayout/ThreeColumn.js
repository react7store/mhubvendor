/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { Text, TouchableOpacity, I18nManager } from 'react-native'
import TimeAgo from '@custom/react-native-timeago'
import { WishListIcon, Rating, ImageCache, ProductPrice } from '@components'
import css from './style'

export default class ThreeColumn extends PureComponent {
  static propTypes = {
    post: PropTypes.object,
    title: PropTypes.string,
    type: PropTypes.string,
    imageURL: PropTypes.string,
    date: PropTypes.any,
    viewPost: PropTypes.func,
  }

  render() {
    const { viewPost, title, post, type, store, viewVendor, imageURL, date } = this.props

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={css.panelThree}
        onPress={viewPost}>
        <ImageCache uri={imageURL} style={css.imagePanelThree} />

        <Text style={css.nameThree}>{title}</Text>
        {typeof type !== 'undefined' && (
          <Text style={css.timeThree}>
            <TimeAgo time={date} />{' '}
          </Text>
        )}
        {typeof type === 'undefined' && (
          <ProductPrice product={post} hideDisCount />
        )}
        {typeof type === 'undefined' && (
          <WishListIcon
            product={post}
            style={I18nManager.isRTL ? { left: 10 } : { right: 20 }}
          />
        )}
        {typeof type === 'undefined' && (<TouchableOpacity onPress={viewVendor}>
          <Text style={css.vendorName}>{store.toUpperCase()}</Text>
        </TouchableOpacity>)}
        {typeof type === 'undefined' && <Rating rating={Number(post.average_rating)}/>}
      </TouchableOpacity>
    )
  }
}
