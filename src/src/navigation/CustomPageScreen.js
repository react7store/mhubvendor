/** @format */

import React, { PureComponent } from 'react'
import { WebView } from 'react-native'
import { View } from 'react-native'
import { Menu, EmptyView } from './IconNav'
import { Languages, Color, Styles } from '@common'
import { CustomPage } from '@containers'
import {warn} from '@app/Omni'

export default class CustomPageScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: Menu(),
    headerRight: EmptyView(),

    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    // headerTitleStyle: Styles.Common.headerStyle,
    title: navigation.state.params.title
})
  
  render() {
    const { state } = this.props.navigation
    return (
      <View style={{flex: 1}}>
        <CustomPage id={state.params.id} />
      </View>
    )
  }
}
