import React, { PureComponent } from "react";
import { Menu, Back, EmptyView } from './IconNav'
import {Text} from 'react-native'
import { TabBarIcon } from '@components'
import { Login } from "@containers";
import { warn } from "@app/Omni"
import {Color, Languages, Styles} from '@common'


export default class LoginScreen extends PureComponent {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: Back(navigation),
        headerRight: EmptyView(),

        headerTintColor: Color.headerTintColor,
        headerStyle: Styles.Common.toolbar,
        // headerTitleStyle: Styles.Common.headerStyle,
        title: Languages.SignIn
    })

    render() {
        const { navigate, state, goBack } = this.props.navigation;
        const isLogout = state.params ? state.params.isLogout : false;

        return <Login statusBar={true}
            navigation={this.props.navigation}
            onBack={goBack}
            isLogout={isLogout}
            onViewSignUp={(user) => navigate('SignUpScreen', user)}
            onViewCartScreen={() => navigate('CartScreen')}
            onViewHomeScreen={() => navigate('Home')}
        />
    }
}
