/** @format */

import React, { PureComponent } from 'react'

import { Color, Images, Styles } from '@common'
import { Cart } from '@containers'

export default class CartScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    header: null,
  })

  render() {
    const { navigate, goBack } = this.props.navigation
    // const rootNavigation = this.props.screenProps.rootNavigation;

    return (
      <Cart
        onMustLogin={() => navigate('LoginScreen', { onCart: true })}
        onBack={() => navigate('Default')}
        onFinishOrder={() => navigate('MyOrders')}
        onViewHome={() => navigate('Default')}
        onViewProduct={(product) => navigate('Detail', product)}
        navigation={this.props.navigation}
      />
    )
  }
}
