/** @format */

import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { ProductVendor } from '@components'
import {warn} from '@app/Omni'


export default class VendorScreen extends PureComponent {
  static navigationOptions = ({ navigation }) => ({
    header: null,
    tabBarVisible: false,
  })

  static propTypes = {
    navigation: PropTypes.object.isRequired,
  }

  render() {
    const { getParam, navigate } = this.props.navigation
    const vendor = getParam('store')

    return (
      <ProductVendor
        vendor={vendor}
        navigation={this.props.navigation}
        onViewVendor={(store) => navigate('Vendor', {store})}
        onViewProductScreen={(item) => navigate('DetailScreen', item)}
    />
    )
  }
}
