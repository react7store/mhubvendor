/** @format */

'use strict'
import React, { Component } from 'react'
import {
  View,
  Image,
  Text,
  FlatList,
  TextInput,
  ScrollView,
} from 'react-native'
import styles from './styles'
import { Constants, Images, Tools, Config, Languages } from '@common'
import HorizonItemSearch from './HorizonItemSearch'
import VerticalItemSearch from './VerticalItemSearch'
import VendorItem from './VendorItem'

export default class DefaultSearch extends Component {

  _renderHeader = (title) => {
    return (
      <View style={styles.headerTitle}>
        <Text style={styles.headerText}>{title}</Text>
      </View>
    )
  }

  _keyExtractor = (item, index) => index.toString()

  render() {
    const { onViewProductScreen, onViewVendor, listCategories, listProducts, listVendors } = this.props
    let list = listCategories.filter(category => category.parent === 0)
    return (
      <ScrollView>
        {this._renderHeader(Languages.Vendors)}
        <FlatList
          data={listVendors}
          horizontal={true}
          keyExtractor={this._keyExtractor}
          renderItem={({ item, index }) => (
            <VendorItem
              key={index}
              data={item}
              onViewVendor={() => onViewVendor(item)}
              width={83}
              height={83}
            />
          )}
        />
        {this._renderHeader(Languages.Categories)}
        <FlatList
          data={list}
          horizontal={true}
          keyExtractor={this._keyExtractor}
          renderItem={({ item, index }) => (
            <HorizonItemSearch
              key={index}
              data={item}
              onViewProductScreen={() => onViewProductScreen(item, index, true)}
              onViewVendor={() => onViewVendor(item.store)}
              width={160}
              height={220}
            />
          )}
        />
        {this._renderHeader(Languages.recents)}
        <FlatList
          data={listProducts}
          horizontal={false}
          keyExtractor={this._keyExtractor}
          renderItem={({ item, index }) => (
            <VerticalItemSearch
              key={index}
              data={item}
              onViewProductScreen={() => onViewProductScreen(item, index)}
              onViewVendor={() => onViewVendor(item.store)}
              width={null}
              height={180}
              styleImage={{
                marginRight: 8,
              }}
            />
          )}
        />
      </ScrollView>
    )
  }
}
