/** @format */

// @flow
/**
 * Created by React7Press on 19/02/2017.
 */
import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { Constants } from '@common'
import { HorizonList, ModalLayout, PostList } from '@components'
import styles from './styles'

class Home extends PureComponent {
  static propTypes = {
    fetchAllCountries: PropTypes.func.isRequired,
    layoutHome: PropTypes.any,
    onViewProductScreen: PropTypes.func,
    onShowAll: PropTypes.func,
  }

  componentDidMount() {
    this.props.fetchAllCountries()
  }

  render() {
    const { layoutHome, onViewVendor, onViewProductScreen, onShowAll } = this.props
    const isHorizontal = layoutHome === Constants.Layout.horizon
    return (
      <View style={styles.container}>
        {isHorizontal && (
          <HorizonList
            onShowAll={onShowAll}
            onViewVendor={onViewVendor}
            onViewProductScreen={onViewProductScreen}
          />
        )}
        {!isHorizontal && (
          <PostList onViewProductScreen={onViewProductScreen} onViewVendor={onViewVendor}/>
        )}
        <ModalLayout />
      </View>
    )
  }
}

const mapStateToProps = ({ user, products }) => ({
  user,
  layoutHome: products.layoutHome,
})

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { dispatch } = dispatchProps
  const CountryRedux = require('@redux/CountryRedux')
  return {
    ...ownProps,
    ...stateProps,
    fetchAllCountries: () => CountryRedux.actions.fetchAllCountries(dispatch),
  }
}

export default connect(mapStateToProps, undefined, mergeProps)(Home)
